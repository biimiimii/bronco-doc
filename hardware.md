# Hardware

![](/assets/IMG_20171116_000737.jpg)Bronco is designed to replace the electrical systems on a standard RC car.

The program accepts remote control input from the PPM header on the Dark Water Escape servo and ESC control board. The car's steering servo and motor controller are connected to the PWM outputs from the Escape. The RoverHat PCB provides connections for 7 external I2C sensors \(5 distance + 2 standard\), 4 LEDs, and a warning buzzer. The BerryGPS-IMU provides the GPS, a LSM9DS0 9-axis IMU, and a BMP280 pressure sensor.



## Bill of Materials


## Power Input

The standard combination of hardware provides a number of options for powering the electronics.

* USB battery pack connected to Raspberry Pi's normal micro USB input
* LiPo battery hat or UPS designed for Raspberry Pi
* 5VIn header on RoverHat\*
* 5V in to Escape's servo rail with power jumper connected\*
* 5V in from speed controller's BEC to PPM input\*

_\*Note: The last three options bypass the input protection polyfuse on the Pi, so input must be somewhat safe and connecting power backwards could destroy the Pi._

## Performance: Pi 3 vs Pi Zero


Pi                | Startup time (s)
----------------- | ----------------
Raspberry Pi 3    | <25s
Raspberry Pi Zero | 53s