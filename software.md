# Software

The inital goal of bronco was to develop an embedded Linux application to run on
a Raspberry Pi that fulfils the two vehicle safety feature requirements for the
4x4 in Schools competition. Furthermore, it should provide a modular and
extensible interface for both input and output to enable the quick addition of
new sensors or sensor types and to allow the incoming sensor data to be
processed and utilised for multiple tasks by the program. By implementing this,
bronco is a complete vehicle management system.

---

## Architecture

In order to read from each sensor at the appropriate rate, every sensor is read
from its own thread. When new data is available, the `SensorState` object is
sent to the queue through the thread-safe `enqueue()` method of `BroncoInput`.

When the BroncoInput thread sees new `SensorState` objects in the queue, it
notifies all of the `SensorUpdateDelegate`s.

To enable the drive modes to be switched, `DriveController`

---

## BroncoInput

### Sensors

## Drive
