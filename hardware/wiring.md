# Wiring

The RoverHat PCB is designed to simplify the car's wiring by giving each sensor a dedicated header. This allows DuPont-style connectors to  be used to make cables that go between the externally mounted sensors and the headers on the PCB.

## Complete Wiring Diagram

![](/assets/wiring.svg)