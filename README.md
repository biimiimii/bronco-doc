# bronco
Wiki for bronco, a telemetry and autonomous control program for RC cars.

[![Wiki Build Status](https://gitlab.com/biimiimii/bronco-doc/badges/master/pipeline.svg)](https://gitlab.com/biimiimii/bronco-doc/commits/master)

---

## Build

bronco is built with CMake and requires WiringPi and pigpio.

```
git clone --recursive git@gitlab.com:biimiimii/bronco.git
cd bronco/cmake
cmake ..
make
sudo ./bronco
```

## [Hardware](hardware.md)

Recommended hardware:
- Raspberry Pi 3 or Zero W
- [RoverHat Sensor Breakout](https://gitlab.com/biimiimii/RoverHat)
- [BerryGPS-IMU](http://ozzmaker.com/product/berrygps-imu/)
- [darkwater ESCAPE](https://darkwater.io/product/escape-esc-powered-motor-control-board/)
- [Adafruit Lux Sensor](https://www.adafruit.com/product/439)
- [Adafruit Distance Sensor](https://www.adafruit.com/product/3317)
- RC receiver with PPM output or PPM converter