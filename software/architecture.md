# Architecture



![Architecture diagram](/assets/architecture.svg)
*UML diagram of bronco showing all classes except for low-level sensor communication libraries and main class.
Open arrows show inheritance, filled diamonds show composition, and dotted lines show indirect relationships.*

