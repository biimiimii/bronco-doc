# Continuous Integration/Continuous Deployment

bronco is distributed as a docker container. 
The bronco docker container is automatically rebuilt every time new code is
pushed to the repository.

---

## Deployment

Before you begin, install the latest version of Raspbian Lite to your SD card.

### Step 1: Install docker

Note: you will need to log out and back in after adding the user pi to docker.

```
curl -sSL https://get.docker.io | sudo sh
sudo usermod -aG docker pi
```

### Step 2: Download the latest version

Builds are stored in the private gitlab registry, so you need to log in (only
once) to download bronco. Replace latest with master to get unstable builds.

```
docker login registry.gitlab.com
docker pull registry.gitlab.com/biimiimii/bronco:latest
```

### Step 3: Run GPSD

Bronco requires GPSD to work. Because the container is run with --net host, GPSD
can be run as a service on the host or as a container.

**Container**
```
docker run -d -p 2947:2947 --name gpsd --device=/dev/ttyAMA0 --restart always forcedinductionz/docker-gpsd -D2 /dev/ttyAMA0
```

**Host**
```
sudo apt install gpsd
sudo systemctl enable gpsd.socket
sudo systemctl start gpsd.socket
```

### Step 4: Enable bronco

This starts bronco detatched from the terminal so you can't see the output.
To see output, replace -d with -it, run `docker attach bronco`, or run `docker
logs bronco`. This will create a container that runs as long as docker is
running.

```
docker run -d --name bronco --init --privileged --net host --restart always registry.gitlab.com/biimiimii/bronco:latest
```

---

## Other setup

- Flash Raspbian Jessie Lite
- `raspi-config`
  - Change password
  - Set locale to `en_US.UTF-8`
  - Set hostname
  - Set WiFi country
  - Enable SSH
  - Enable I2C
  - Disable login over serial, enable serial port `no/yes`
- Fix keyboard
  - `sudo dpkg-reconfigure keyboard-configuration`
  - `invoke-rc.d keyboard-setup start`
- Reboot
- Add wifi networks
  - `wpa_passphrase "SSID" "psk" >> wpa.conf`
- Copy ssh keys
  - `ssh-copy-id pi@IP_ADDR`
  

---

## Issues

- Bronco doesn't receive the `SIGTERM` when docker stop is used to halt the
  program. This causes pigpio cleanup not run, leading to initialization failing
  as soon as docker tries to restart it.

- Container is run with --net host. From a containerization perspective, this is
  a bad idea, but it allows telemetry to be accessible outside of the host.
  
- Backup: https://raspberrypi.stackexchange.com/questions/5427/can-a-raspberry-pi-be-used-to-create-a-backup-of-itself

rsync-ignore.txt
```
pi@turbotigers:~ $ cat rsync-ignore.txt
/proc/*
/sys/*
/dev/*
/boot/*
/tmp/*
/run/*
/mnt/*
/media/*
```

```
sudo mkdir /mnt/rootfs/
sudo mount /dev/sda2 /mnt/rootfs/
sudo mount /dev/sda1 /mnt/rootfs/boot/
sudo rsync -aHv --delete-during --exclude-from=rsync-ignore.txt / /mnt/rootfs/
```

sudo rsync -aHv --delete-during --exclude-from=rsync-ignore.txt / /mnt/rootfs/
- https://stackoverflow.com/questions/47520693/raspbian-docker-error-response-from-daemon-cgroups-memory-cgroup-not-supporte

- UUID in /etc/fstab and /boot/cmdline.txt

- Wireless possibly broken in stretch: https://www.raspberrypi.org/forums/viewtopic.php?t=191252