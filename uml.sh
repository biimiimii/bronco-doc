# Shell script to generate PlantUML diagrams
puml generate -s -o "assets/wiring.svg" "hardware/wiring.uml"
puml generate -s -o "assets/architecture.svg" "software/architecture.uml"